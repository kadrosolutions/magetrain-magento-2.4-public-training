<?php
namespace Training\John\Api\Data;

interface TribbleInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param @id
     * @return void
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $name
     * @return void
     */
    public function setName($name);
}