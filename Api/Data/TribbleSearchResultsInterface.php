<?php
namespace Training\John\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface TribbleSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get blocks list.
     *
     * @return \Training\John\Api\Data\TribbleInterface[] $items
     */
    public function getItems();

    /**
     * Set blocks list.
     *
     * @param \Training\John\Api\Data\TribbleInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
