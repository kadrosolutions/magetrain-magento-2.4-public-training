<?php
namespace Training\John\Api;

use Training\John\Api\Data\TribbleInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

interface TribbleRepositoryInterface
{
    /**
     * Saves a tribble.
     *
     * @param TribbleInterface $tribble
     * @return TribbleInterface
     * @throws CouldNotSaveException
     */
    public function save(TribbleInterface $tribble);

    /**
     * Loads the tribble by id
     *
     * @param $tribbleId
     * @return TribbleInterface
     * @throws NoSuchEntityException
     */
    public function getById($tribbleId);

    /**
     * Delete a tribble.
     *
     * @param TribbleInterface $tribble
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(TribbleInterface $tribble);

    /**
     * Delete a tribble by id.
     *
     * @param $tribbleId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($tribbleId);

    /**
     * Retrieve tribble list matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Training\John\Api\Data\TribbleSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
