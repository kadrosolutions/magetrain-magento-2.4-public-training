<?php
namespace Training\John\Block;

class HelloWorld extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Training\John\Api\Data\TribbleInterfaceFactory
     */
    protected $tribbleFactory;

    /**
     * @var \Training\John\Api\TribbleRepositoryInterface
     */
    protected $tribbleRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Training\John\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Training\John\Api\Data\TribbleInterfaceFactory $tribbleFactory
     * @param \Training\John\Api\TribbleRepositoryInterface $tribbleRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Training\John\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Training\John\Api\Data\TribbleInterfaceFactory $tribbleFactory,
        \Training\John\Api\TribbleRepositoryInterface $tribbleRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Training\John\Helper\Data $helper,
        array $data
    ) {
        parent::__construct($context, $data);
        $this->tribbleFactory = $tribbleFactory;
        $this->tribbleRepository = $tribbleRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->eventManager = $eventManager;
        $this->helper = $helper;
    }

    public function getMessage()
    {
        $message = $this->_scopeConfig->getValue('training_john/configuration/message');;
        $this->eventManager->dispatch(
            'training_john_get_message_event',
            array(
                'message' => $message
            )
        );
        return $message . ' ' . $this->helper->foo();
    }

    public function getTribbles()
    {

        /**
         * Get Tribble with the name Max, if it does not exist, create it and save it
         */
        $nameSearch = $this->searchCriteriaBuilder->addFilter('name', 'max')->create();
        $maxTribbles = $this->tribbleRepository->getList($nameSearch);
        if ($maxTribbles->getTotalCount() == 0) {
            $max = $this->tribbleFactory->create();
            $max->setName('Max');
            $this->tribbleRepository->save($max);
        }

        /**
         * Getting the existing tribbles
         */
        $noParamSearch = $this->searchCriteriaBuilder->create();
        $existingTribbles = $this->tribbleRepository->getList($noParamSearch);

        /**
         * We're done with Max, delete him!
         */
        try {
            $this->tribbleRepository->delete($max);
        } catch (\Magento\Framework\Exception\CouldNotDeleteException $couldNotDeleteException) {
            /**
             * This may happen if someone else deleted max before you managed to, but it's okay
             * we can catch the exception and move on.
             */
        }

        /**
         * Even though Max was deleted from the database he's probably still in here cause he was there
         * when we got the tribbles.
         */
        return $existingTribbles->getItems();
    }
}
