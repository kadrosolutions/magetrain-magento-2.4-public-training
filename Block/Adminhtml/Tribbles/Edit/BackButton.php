<?php
namespace Training\John\Block\Adminhtml\Tribbles\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\App\RequestInterface;

class BackButton implements ButtonProviderInterface
{
    protected $context;
    protected $request;

    public function __construct(
        Context $context,
        RequestInterface $request
    ) {
        $this->context = $context;
        $this->request = $request;
    }

    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'class' => 'back',
            'sort_order' => 10
        ];
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/*/index');
    }

    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
