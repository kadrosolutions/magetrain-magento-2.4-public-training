<?php
namespace Training\John\Block\Adminhtml\Tribbles\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
    }

    public function _prepareForm()
    {
        $tribble = $this->_coreRegistry->registry('tribble');
        $form = $this->_formFactory->create(
            array(
                'data' => array(
                    'id' => 'edit_form',
                    'method' => 'post',
                    'action' => $this->getData('action')
                )
            )
        );
        $fieldset = $form->addFieldset(
            'tribble_details',
            array('legend' => __('Tribble Details'), 'class' => 'fieldset-wide')
        );
        if($tribble->getId() != null) {
            $fieldset->addField(
                'id',
                'hidden',
                array('name' => 'id')
            );
        }
        $fieldset->addField(
            'name',
            'text',
            array('label' => __('Name'), 'required' => true, 'name' => 'name')
        );
        $form->setUseContainer(true);
        $form->setValues($tribble->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
