<?php
namespace Training\John\Block\Adminhtml\Tribbles\Grid;

class Container extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_tribbles';
        $this->_headerText = __('Tribbles');
        $this->_addButtonLabel = __('Add New Tribble');
        parent::_construct();
    }
}
