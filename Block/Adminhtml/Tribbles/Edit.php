<?php
namespace Training\John\Block\Adminhtml\Tribbles;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->registry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        parent::_construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'Training_John';
        $this->_controller = 'adminhtml_tribbles';

        $this->buttonList->update('save', 'label', __('Save Tribble'));
        $this->buttonList->update('save', 'id', 'save_button');
        $this->buttonList->update('delete', 'label', __('Delete Tribble'));
    }

    public function getHeaderText()
    {
        $tribble = $this->registry>registry('tribble');
        if ($tribble && $tribble->getId()) {
            return __("Edit Tribble '%1'", $this->escapeHtml($tribble->getName()));
        } else {
            return __('New Tribble');
        }
    }
}
