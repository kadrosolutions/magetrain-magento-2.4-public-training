<?php
namespace Training\John\Model\ResourceModel\Tribble;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_cacheTag = 'training_john_tribble';
    /**
     * @var string
     */
    protected $_eventPrefix = 'training_john_tribble';

    protected function _construct()
    {
        $this->_init('Training\John\Model\Tribble', 'Training\John\Model\ResourceModel\Tribble');
    }
}
