<?php
namespace Training\John\Model\ResourceModel;

class Tribble extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('training_tribble', 'id');
    }
}
