<?php
namespace Training\John\Model;

class Tribble extends \Magento\Framework\Model\AbstractModel implements \Training\John\Api\Data\TribbleInterface
{
    /**
     * @var string
     */
    protected $_cacheTag = 'training_john_tribble';
    /**
     * @var string
     */
    protected $_eventPrefix = 'training_john_tribble';

    protected function _construct()
    {
        $this->_init('Training\John\Model\ResourceModel\Tribble');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return parent::getId();
    }

    /**
     * @param $id
     * @return void
     */
    public function setId($id)
    {
        parent::setId($id);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * @param $name
     * @return void
     */
    public function setName($name)
    {
        $this->setData('name', $name);
    }
}