<?php
namespace Training\John\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

class TribbleRepository implements \Training\John\Api\TribbleRepositoryInterface
{
    /**
     * @var ResourceModel\Tribble
     */
    protected $resource;

    /**
     * @var TribbleFactory
     */
    protected $factory;

    /**
     * @var ResourceModel\Tribble\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var \Training\John\Api\Data\TribbleSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @param ResourceModel\Tribble $resource
     * @param TribbleFactory $factory
     * @param ResourceModel\Tribble\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor
     * @param \Training\John\Api\Data\TribbleSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Training\John\Model\ResourceModel\Tribble $resource,
        \Training\John\Model\TribbleFactory $factory,
        \Training\John\Model\ResourceModel\Tribble\CollectionFactory $collectionFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor,
        \Training\John\Api\Data\TribbleSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource = $resource;
        $this->factory = $factory;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Save a trobb;e
     *
     * @param \Training\John\Api\Data\TribbleInterface $tribble
     * @return \Training\John\Api\Data\TribbleInterface
     * @throws CouldNotSaveException
     */
    public function save(\Training\John\Api\Data\TribbleInterface $tribble)
    {
        try {
            $this->resource->save($tribble);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $tribble;
    }

    /**
     * Get a tribble by id
     *
     * @param $id
     * @return ExportMessage
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $tribble = $this->factory->create();
        $this->resource->load($tribble, $id);
        if (!$tribble->getId()) {
            throw new NoSuchEntityException(__('Tribble with Id "%1" does not exist.', $id));
        }
        return $tribble;
    }

    /**
     * Delete a tribble
     *
     * @param \Training\John\Api\Data\TribbleInterface $tribble
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Training\John\Api\Data\TribbleInterface  $tribble)
    {
        try {
            $this->resource->delete($tribble);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__($e->getMessage()));
        }
        return true;
    }

    /**
     * Delete a tribble by id.
     *
     * @param $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * @param  \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Training\John\Api\Data\TribbleSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setSearchCriteria($searchCriteria);
        return $searchResult;
    }
}