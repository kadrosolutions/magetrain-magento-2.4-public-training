<?php
namespace Training\John\Model;
use Training\John\Api\Data\TribbleInterface;
class TribbleStorage
{
    protected $tribble;
    public function setTribble(TribbleInterface $tribble)
    {
        $this->tribble = $tribble;
    }

    public function getTribble()
    {
        return $this->tribble;
    }
}
