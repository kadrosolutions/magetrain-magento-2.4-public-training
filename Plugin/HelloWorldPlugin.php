<?php
namespace Training\John\Plugin;

class HelloWorldPlugin
{
    public function beforeGetTribbles(
        \Training\John\Block\HelloWorld $subject
    ) {
        echo 'Hello from the Before Plugin Function';
    }
}
