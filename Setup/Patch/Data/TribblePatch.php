<?php
namespace Training\John\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class TribblePatch implements DataPatchInterface
{
    private $moduleDataSetup;
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $data = array(
            array(
                'name' => 'Fuzzy'
            ),
            array(
                'name' => 'Picard Jr.'
            )
        );foreach($data as $item) {
        $this->moduleDataSetup->getConnection()->insert('training_tribble', $item);
    }
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

}
