<?php
namespace Training\John\Observer;

class LogMessage implements \Magento\Framework\Event\ObserverInterface
{
    protected $logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $this->logger->info($observer->getMessage());
    }
}
