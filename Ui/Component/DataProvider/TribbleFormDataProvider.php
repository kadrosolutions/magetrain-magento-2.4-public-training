<?php
namespace Training\John\Ui\Component\DataProvider;

use Magento\Framework\Api\Filter;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider;
use Training\John\Model\TribbleStorage;

class TribbleFormDataProvider extends DataProvider
{

    protected $tribbleStorage;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        ReportingInterface $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        TribbleStorage $tribbleStorage,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
        $this->tribbleStorage = $tribbleStorage;
    }

    public function getData()
    {
        if ($this->tribbleStorage->getTribble() !== null) {
            return [$this->tribbleStorage->getTribble()->getId() => [
                'id' => $this->tribbleStorage->getTribble()->getId(),
                'name' => $this->tribbleStorage->getTribble()->getName()
            ]];
        }
        return [];
    }

    public function addFilter(Filter $filter)
    {
        return;
    }
}
