<?php
namespace Training\John\Controller\Adminhtml\Tribbles;

class Save extends \Magento\Backend\App\Action
{
    protected $tribbleFactory;
    protected $tribbleRepository;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Training\John\Model\TribbleFactory $tribbleFactory,
        \Training\John\Api\TribbleRepositoryInterface $tribbleRepository
    ) {
        $this->tribbleFactory = $tribbleFactory;
        $this->tribbleRepository = $tribbleRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if($data) {
            if (empty($data['id'])) {
                $data['id'] = null;
            }
            $id = $this->getRequest()->getParam('id');
            if($id != null) {
                try {
                    $tribble = $this->tribbleRepository->getById($id);
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
                    $this->_getSession()->setFormData($data);
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
                }
            } else {
                $tribble = $this->tribbleFactory->create();
            }

            $tribble->setData($data);

            $this->_eventManager->dispatch(
                'tribble_prepare_save',
                array('tribble' => $tribble, 'request' => $this->getRequest())
            );
            try {
                $this->tribbleRepository->save($tribble);
                $this->messageManager->addSuccess(__('Tribble Saved'));
                $this->_getSession()->setFormData(false);
                if($this->getRequest()->getParam('back') != null) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $tribble->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the tribble'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
