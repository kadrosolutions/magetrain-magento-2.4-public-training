<?php
namespace Training\John\Controller\Adminhtml\Tribbles;

class Delete extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $tribbleFactory;
    protected $tribbleRepository;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Training\John\Model\TribbleFactory $tribbleFactory,
        \Training\John\Api\TribbleRepositoryInterface $tribbleRepository
    ) {
        $this->tribbleFactory = $tribbleFactory;
        $this->tribbleRepository = $tribbleRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if($id != null) {
            try {
                $this->tribbleRepository->deleteById($id);
                $this->messageManager->addSuccess(__('Tribble deleted.'));
                return $this->resultRedirectFactory->create()->setPath('*/*/');
            } catch(\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $this->resultRedirectFactory->create()->setPath('*/*/edit', array('id' => $id));
            }
        }
        $this->messageManager->addError('Tribble does not exist.');
        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
}
