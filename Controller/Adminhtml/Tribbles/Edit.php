<?php
namespace Training\John\Controller\Adminhtml\Tribbles;

class Edit extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $tribbleFactory;
    protected $tribbleRepository;
    protected $registry;
    protected $tribbleStorage;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Training\John\Model\TribbleFactory $tribbleFactory,
        \Training\John\Api\TribbleRepositoryInterface $tribbleRepository,
        \Training\John\Model\TribbleStorage $tribbleStorage
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->tribbleFactory = $tribbleFactory;
        $this->tribbleRepository = $tribbleRepository;
        $this->tribbleStorage = $tribbleStorage;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        if($id != null) {
            try {
                $tribble  = $this->tribbleRepository->getById($id);
            } catch(\Exception $e) {
                $this->messageManager->addError(__('This tribble could not be loaded'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');

            }
        } else {
            $tribble = $this->tribbleFactory->create();
        }

        $this->tribbleStorage->setTribble($tribble);

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu("Training_John::tribbles_view");
        $resultPage->getConfig()->getTitle()->prepend(__('Tribbles'));
        $resultPage->getConfig()->getTitle()->prepend(__('Edit Tribble'));
        return $resultPage;
    }
}
