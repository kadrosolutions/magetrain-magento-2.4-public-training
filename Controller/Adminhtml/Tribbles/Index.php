<?php
namespace Training\John\Controller\Adminhtml\Tribbles;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Training_John::tribbles_view';

    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu("Kadro_Training::tribbles_view");
        $resultPage->getConfig()->getTitle()->prepend(__('Tribbles'));
        return $resultPage;
    }
}
