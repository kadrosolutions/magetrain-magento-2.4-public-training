<?php
namespace Training\John\Controller\Exercise;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $viewHelper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
